export const requestErrors = {
  unauthorized: {
    title: `Opss...`,
    message: `Hummm, it looks like you are not allowed to make a mistake request.`
  },
  notFound: {
    title: `Opss...`,
    message: `Hummm, it looks like the route you want to access doesn't exist.`
  }
};

